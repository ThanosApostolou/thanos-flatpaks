## Use Repo
```
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --if-not-exists thanos-flatpaks https://thanosapostolou.gitlab.io/thanos-flatpaks/thanos-flatpaks.flatpakrepo
```

## Apps
  - org.gyunaev.Birdtray (https://github.com/gyunaev/birdtray)
  - org.xgraph.Xgraph (http://www.xgraph.org)

## Build Package
```
cd <name>
flatpak-builder --force-clean --repo=../public --gpg-sign=thanosapostolou@outlook.com build <name>.json
cd ..
flatpak build-update-repo --generate-static-deltas --gpg-sign=thanosapostolou@outlook.com public
```


#### For 32bit (optional)
Change runtimes to latest supported versions:
  - org.freedesktop.Paltform 18.08
  - org.kde.Platform 5.12
  - org.gnome.Platform 3.32

```
flatpak-builder --force-clean --repo=../public --gpg-sign=thanosapostolou@outlook.com --arch=i386 build <name>.json
```

## Initialize Repo
Create gpg key with `gpg --full-gen-key`

Create thanos-flatapks.flatpakrepo based on flathub.flatpakrepo

see your key id with `gpg -k` (e.g. D4D63...)

add to the line *GPGKey=* of thanos-flatapks.flatpakrepo the output of `gpg --armor --export D4D63...| base64 --wrap=0`
